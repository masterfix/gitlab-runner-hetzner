ARG RUNNER_VERSION

FROM gitlab/gitlab-runner:alpine-v${RUNNER_VERSION}

ARG HETZNER_VERSION

RUN wget "https://github.com/JonasProgrammer/docker-machine-driver-hetzner/releases/download/${HETZNER_VERSION}/docker-machine-driver-hetzner_${HETZNER_VERSION}_linux_amd64.tar.gz" && \
    tar -xf docker-machine-driver-hetzner_${HETZNER_VERSION}_linux_amd64.tar.gz docker-machine-driver-hetzner && \
    chmod +x docker-machine-driver-hetzner && \
    mv docker-machine-driver-hetzner /usr/local/bin/ && \
    rm -f docker-machine-driver-hetzner_${HETZNER_VERSION}_linux_amd64.tar.gz